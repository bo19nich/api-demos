from db import db


class ItemModel(db.Model):
    __tablename__ = 'items'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    price = db.Column(db.Float(precision=2))

    # a foreignkey inherits a primary key from another table
    # we want every item to be linked to a store id
    store_id = db.Column(db.Integer, db.ForeignKey('stores.id'))

    # so we can identify the store that matches the
    # contains the item (join)
    store = db.relationship('StoreModel')

    def __init__(self, name, price, store_id):
        self.name = name
        self.price = price
        self.store_id = store_id

    def json(self):
        """
        :return: convert ItemModel object into json
        """
        return {'name': self.name, 'price': self.price, 'store_id': self.store_id}

    @classmethod
    def find_by_name(cls, name):
        """
        objective: finds an item in the database by name
        :param name: str - item name
        :return: json - row data in object form, None otherwise
        """
        return cls.query.filter_by(name=name).first()

    def save_to_db(self):
        """
        objective: insert item to database if nonexisting
                   or otherwise update the database
        :return: None
        """
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        """
        objective: remove contents from database if existing
        :return: None
        """
        db.session.delete(self)
        db.session.commit()


class ItemModel(db.Model):
    __tablename__ = 'items'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    price = db.Column(db.Float(precision=2))

    def __init__(self, name, price):
        self.name = name
        self.price = price

    def json(self):
        """
        :return: convert ItemModel object into json
        """
        return {'name': self.name, 'price': self.price}

    @classmethod
    def find_by_name(cls, name):
        """
        objective: finds an item in the database by name
        :param name: str - item name
        :return: json - row data in object form, None otherwise
        """
        # sql alchemy can handle:
        #   - connection to database file
        #   - cursor object
        #   - queries
        #   - database row to object creation
        #   - object to row translation in database

        # equivalent to
        #   SELECT * FROM items WHERE name=name LIMIT 1
        # and then returns an ItemModel object
        return cls.query.filter_by(name=name).first()

    # this was formally the insert method
    # we remained to save_to_db because
    # it now acts as both an insert an update method
    # functionality is similar to PUT
    def save_to_db(self):
        """
        objective: insert item to database if nonexisting
                   or otherwise update the database
        :return: None
        """
        # we can also add multiple objects
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        """
        objective: remove contents from database if existing
        :return: None
        """
        db.session.delete(self)
        db.session.commit()