API DEMO
===

Introduction
---

To run this app you are supposed to be having the basic knowledge of python and flask

This sample api app is a flask project that demostrates a simple store managemet 

It demostrates how an authorised user can add items in the store and also view the available items in the store.

Requirements
---
* Python3 - A programming language that lets you work more quickly (The universe loves speed!).
* Flask - A microframework for Python based on Werkzeug, Jinja 2 and good intentions
* Virtualenv - A tool to create isolated virtual environments
* Sqlite – SQLite database for testing datastorage in the database

Installation
---
* If you wish to run your own build, first ensure you have python3 globally installed in your computer.
* After this, ensure you have installed virtualenv globally as well. If not, run this:

   $ pip install Virtualenv

* Git clone this repo to your PC
    
   $ git clone git@gitlab.com/bo19nich/api-demos
   
**Dependences**
1. Cd into your the cloned repo as such:

    $ cd api-demo
2. Create and fire up your virtual environment in python3

    $ virtualenv -p python3 venv

* Running it
    On your terminal, run the server using this one simple command:
    
    (venv)$ flask run
    
    You can now access the app on your local browser by using
    
    http://localhost:5000/
    
    Or test using Postman


Contributer
---
Nicholas Bonny

