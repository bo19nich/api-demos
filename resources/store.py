from models.store import StoreModel
from flask_restful import Resource


class Store(Resource):
    def get(self, name):
        store = StoreModel.find_by_name(name)
        # default status code return is 200
        return store.json() if store else {'message': f'A store with the name {name} is not found.'}, 404

    def post(self, name):
        if StoreModel.find_by_name(name):
            return {'message': f'A store with the name {name} already exists.'}, 400

        # store doesnt exist, so create one
        store = StoreModel(name)
        try:
            store.save_to_db()
        except:
            return {'message': 'Store failed add into the database.'}, 500
        return store.json(), 201

    def delete(self, name):
        store = StoreModel.find_by_name(name)
        if store:
            store.delete_from_db()
        return {'message': 'Store deleted.'}


class StoreList(Resource):
    def get(self):
        return {'stores': [store.json() for store in StoreModel.query.all()]}